
# Tests with MicroPython on ESP8266 ESP12

  - Docs link:

      * https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/repl.html
      * https://docs.micropython.org/en/latest/esp8266/esp8266/quickref.html


 - Firmware:

       * http://micropython.org/download#esp8266
       * http://micropython.org/resources/firmware/esp8266-20160909-v1.8.4.bin

 - Erase board flash memory:

       * python venv\Lib\site-packages\esptool.py --port COM13 erase_flash

 - Send firmware to board:

       * python venv\Lib\site-packages\esptool.py --port COM13 --baud 460800 write_flash --flash_size=8m 0 esp8266-20160909-v1.8.4.bin

 - REPL:

       * client: http://micropython.org/webrepl/

           - WIFI
              * name: MicroPyrhon XXXX
              * pass: micropythoN